package ru.t1.rleonov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.dto.response.*;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @ SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerLoadDataBackupResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerLoadDataBackupRequest request
    );

    @NotNull
    @WebMethod
    ServerLoadDataBase64Response loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerLoadDataBase64Request request
    );

    @NotNull
    @WebMethod
    ServerLoadDataBinaryResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerLoadDataBinaryRequest request
    );

    @NotNull
    @WebMethod
    ServerLoadDataJsonFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerLoadDataJsonFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    ServerLoadDataJsonJaxBResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerLoadDataJsonJaxBRequest request
    );

    @NotNull
    @WebMethod
    ServerLoadDataXmlFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerLoadDataXmlFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    ServerLoadDataXmlJaxBResponse loadDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerLoadDataXmlJaxBRequest request
    );

    @NotNull
    @WebMethod
    ServerLoadDataYamlFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerLoadDataYamlFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    ServerSaveDataBackupResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerSaveDataBackupRequest request
    );

    @NotNull
    @WebMethod
    ServerSaveDataBase64Response saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerSaveDataBase64Request request
    );

    @NotNull
    @WebMethod
    ServerSaveDataBinaryResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerSaveDataBinaryRequest request
    );

    @NotNull
    @WebMethod
    ServerSaveDataJsonFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerSaveDataJsonFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    ServerSaveDataJsonJaxBResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerSaveDataJsonJaxBRequest request
    );

    @NotNull
    @WebMethod
    ServerSaveDataXmlFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerSaveDataXmlFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    ServerSaveDataXmlJaxBResponse saveDataXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerSaveDataXmlJaxBRequest request
    );

    @NotNull
    @WebMethod
    ServerSaveDataYamlFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ServerSaveDataYamlFasterXmlRequest request
    );

}
