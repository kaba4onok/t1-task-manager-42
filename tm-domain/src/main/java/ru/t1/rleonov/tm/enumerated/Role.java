package ru.t1.rleonov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Role role: values()) {
            if (role.name().equals(value)) return role;
        }
        return null;
    }

}
