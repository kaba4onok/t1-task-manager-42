package ru.t1.rleonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm.user (id, login, password, email, last_name, first_name, middle_name, role, locked) " +
            "VALUES(#{id}, #{login}, #{password}, #{email}, #{lastName}, #{firstName}, #{middleName}, " +
            "#{role}, #{locked});")
    void add(@NotNull UserDTO user);

    @Delete("DELETE FROM tm.user WHERE id = #{id};")
    void removeById(@NotNull @Param("id") String id);

    @Delete("TRUNCATE TABLE tm.user;")
    void clearAll();

    @Update("UPDATE tm.user SET last_name = #{lastName}, first_name = #{firstName}, middle_name = #{middleName} " +
            "WHERE id = #{id};")
    void updateUser(@Nullable @Param("id") String id,
                @Nullable @Param("firstName") String firstName,
                @Nullable @Param("lastName") String lastName,
                @Nullable @Param("middleName") String middleName);

    @Select("SELECT * FROM tm.user WHERE id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @Nullable
    UserDTO findOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm.user;")
    @Results(value = {
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @NotNull
    List<UserDTO> findAll();

    @Select("SELECT * FROM tm.user WHERE login = #{login} LIMIT 1;")
    @Results(value = {
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @Nullable
    UserDTO findOneByLogin(@NotNull @Param("login") String login);

    @Select("SELECT * FROM tm.user WHERE email = #{email} LIMIT 1;")
    @Results(value = {
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name")})
    @Nullable
    UserDTO findByEmail(@NotNull @Param("email") String email);

    @Update("UPDATE tm.user SET password = #{password} WHERE id = #{id};")
    void setPassword(@Nullable @Param("id") String id, @NotNull @Param("password") String password);

    @Update("UPDATE tm.user SET locked = true WHERE id = #{id};")
    void lockUserById(@Nullable @Param("id") String id);

    @Update("UPDATE tm.user SET locked = false WHERE id = #{id};")
    void unlockUserById(@Nullable @Param("id") String id);

}
