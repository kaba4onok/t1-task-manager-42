package ru.t1.rleonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm.task (id, name, description, status, created, user_id, project_id) " +
            "VALUES(#{id}, #{name}, #{description}, #{status}, #{created}, #{userId}, #{projectId});")
    void add(@NotNull TaskDTO task);

    @Delete("DELETE FROM tm.task WHERE id = #{id} AND user_id = #{userId};")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm.task WHERE user_id = #{userId};")
    void clear(@NotNull @Param("userId") String userId);

    @Delete("TRUNCATE TABLE tm.task;")
    void clearAll();

    @Select("SELECT * FROM tm.task;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @NotNull List<TaskDTO> findAllTasks();

    @Select("SELECT * FROM tm.task WHERE user_id = #{userId} ORDER BY #{sort};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @NotNull List<TaskDTO> findAll(@NotNull @Param("userId") String userId, @NotNull @Param("sort") String sort);

    @Select("SELECT * FROM tm.task WHERE id = #{id} AND user_id = #{userId} LIMIT 1;")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @Nullable
    TaskDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Update("UPDATE tm.task SET status = #{status} WHERE id = #{id} AND user_id = #{userId};")
    void changeStatusById(@Nullable @Param("userId") String userId,
                          @Nullable @Param("id") String id,
                          @Nullable @Param("status") Status status);

    @Update("UPDATE tm.task SET name = #{name}, description = #{description} " +
            "WHERE id = #{id} AND user_id = #{userId};")
    void updateById(@Nullable @Param("userId") String userId,
                    @Nullable @Param("id") String id,
                    @Nullable @Param("name") String name,
                    @Nullable @Param("description") String description);

    @Select("SELECT * FROM tm.task WHERE user_id = #{userId} AND project_id = #{projectId};")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")})
    @NotNull List<TaskDTO> findAllByProjectId(@NotNull @Param("userId") String userId,
                                              @NotNull @Param("projectId") String projectId);

    @Update("UPDATE tm.task SET project_id = #{projectId} WHERE id = #{taskId};")
    void bindTaskToProject(@Nullable @Param("projectId") String projectId,
                           @Nullable @Param("taskId") String taskId);

    @Update("UPDATE tm.task SET project_id = null WHERE id = #{taskId};")
    void unbindTaskFromProject(@Nullable @Param("taskId") String taskId);

}
