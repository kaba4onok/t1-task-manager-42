package ru.t1.rleonov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.IProjectService;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import java.util.Collection;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
       this.connectionService = connectionService;
    }

    @Override
    @NotNull
    public String getSortType(@Nullable final Sort sort) {
        if (sort == Sort.BY_CREATED) return "created";
        if (sort == Sort.BY_STATUS) return "status";
        else return "name";
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateById(
            @Nullable String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.updateById(userId, id, name, description);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    @NotNull
    @SneakyThrows
    public ProjectDTO removeById(
            @Nullable String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.removeById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.changeStatusById(userId, id, status);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.clearAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findAll(userId, getSortType(sort));
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return repository.findAllProjects();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable final ProjectDTO project = repository.findOneById(userId, id);
            return project;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void set(@NotNull final Collection<ProjectDTO> projects) {
        if (projects.isEmpty()) return;
        clear();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            for (ProjectDTO project: projects) {
                repository.add(project);
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
