package ru.t1.rleonov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.Domain;

public interface IDomainService {

    @NotNull
    Domain getDomain();

    void setDomain(@Nullable Domain domain);

    @SneakyThrows
    void loadDataBackup();

    @SneakyThrows
    void saveDataBackup();

    @SneakyThrows
    void loadDataBase64();

    @SneakyThrows
    void saveDataBase64();

    @SneakyThrows
    void loadDataBinary();

    @SneakyThrows
    void saveDataBinary();

    @SneakyThrows
    void loadDataJsonFasterXml();

    @SneakyThrows
    void loadDataJsonJaxB();

    @SneakyThrows
    void saveDataJsonFasterXml();

    @SneakyThrows
    void saveDataJsonJaxB();

    @SneakyThrows
    void loadDataXmlFasterXml();

    @SneakyThrows
    void loadDataXmlJaxB();

    @SneakyThrows
    void saveDataXmlFasterXml();

    @SneakyThrows
    void saveDataXmlJaxB();

    @SneakyThrows
    void loadDataYamlFasterXml();

    @SneakyThrows
    void saveDataYamlFasterXml();

}
